//
//  Action.swift
//  ZendeskGuide
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import Foundation
import Suas
import Alamofire
import SwiftyJSON

struct FetchArticlesAsyncAction: AsyncAction {
    let nextPage: String?
    
    init(nextPage: String? = nil) {
        self.nextPage = nextPage
    }
    
    func execute(getState: @escaping GetStateFunction, dispatch: @escaping DispatchFunction) {
        let url = nextPage != nil ? nextPage : "\(Utils.domain)/api/v2/help_center/en-us/sections/200623776/articles.json"
        
        Alamofire.request(url!, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                var articles = [] as [ArticleItem]
                
                let nextPage = json["next_page"].string
                
                for (_, subJson) in json["articles"] {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                    let updatedDate = dateFormatter.date(from:subJson["edited_at"].stringValue)!
                    
                    articles.append(ArticleItem(title: subJson["title"].stringValue,
                                                content: subJson["body"].stringValue,
                                                updatedDate: updatedDate,
                                                nextPage: nextPage))
                }
                
                dispatch(ArticlesFetchedAction(articles: articles, error: false))
                
            case .failure(let error):
                dispatch(ArticlesFetchedAction(articles: [], error: true))
                print(error)
            }
        }
    }
}

//error indicates whether the Web Service calling contains any error
struct ArticlesFetchedAction: Action {
    let articles: [ArticleItem]
    let error: Bool
}

struct ViewArticleAction: Action {
    let article: ArticleItem
}

//Pull to refresh action
struct RefreshArticlesAction: Action {}
