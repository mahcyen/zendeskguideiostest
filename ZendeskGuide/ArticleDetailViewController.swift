//
//  ArticleDetailViewController.swift
//  ZendeskGuide
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import UIKit
import WebKit

class ArticleDetailViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        let selectedArticle = store.state.value(forKeyOfType: SelectedArticle.self)!.article

        titleLabel.text = selectedArticle.title
        dateLabel.text = "Last updated: \(Utils.timeAgoSinceDate(selectedArticle.updatedDate!, numericDates: true)) (\(dateFormatter.string(from: selectedArticle.updatedDate!)))"
        
        webView.loadHTMLString((selectedArticle.content)!, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
