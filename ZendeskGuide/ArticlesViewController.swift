//
//  ArticlesViewController.swift
//  ZendeskGuide
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import UIKit
import Suas

class ArticlesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var articlesTableView: UITableView!
    
    var loadingData = true
    
    let refreshControl = UIRefreshControl()
    
    var state: ArticleList = ArticlesReducer().initialState {
        didSet {
            //If contains error from loading Web Services
            if(state.error) {
                let alert = UIAlertController(title: "Error", message: "Internet connection appears to be offline.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                    self.refreshControl.endRefreshing()
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            loadingData = false
            self.refreshControl.endRefreshing()
            articlesTableView.reloadData()
        }
    }
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = Utils.accentColor
        self.navigationController?.navigationBar.tintColor = Utils.mainColor
        
        let textAttributes = [NSAttributedStringKey.foregroundColor: Utils.mainColor]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        articlesTableView.dataSource = self
        articlesTableView.delegate = self
        
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        
        articlesTableView.refreshControl = refreshControl

        store.addListener(forStateType: ArticleList.self) { [weak self] newState in
            self?.state = newState
            }.linkLifeCycleTo(object: self)
        
        loadData()
    }
    
    private func loadData(nextPage: String? = nil) {
        loadingData = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        store.dispatch(action: FetchArticlesAsyncAction(nextPage: nextPage))
    }
    
    @objc private func refreshTableView(refreshControl: UIRefreshControl) {
        if(!loadingData)
        {
            store.dispatch(action: RefreshArticlesAction())
            loadData()
        }
        else
        {
            self.refreshControl.endRefreshing()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return state.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticlesTableViewCell", for: indexPath) as! ArticlesTableViewCell
        
        let article = state.articles[indexPath.row]
        
        cell.titleLabel.text = article.title
        cell.dateLabel.text = "Last updated: \(Utils.timeAgoSinceDate(article.updatedDate!, numericDates: true))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        store.dispatch(action: ViewArticleAction(article: state.articles[indexPath.row]))
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = state.articles.count - 1
        let lastArticle = state.articles[lastElement]
        
        if !loadingData && indexPath.row == (lastElement - 10) && lastArticle.nextPage != nil && !state.error {
            loadData(nextPage: lastArticle.nextPage)
        }
    }
}
