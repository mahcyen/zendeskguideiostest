//
//  Reducer.swift
//  ZendeskGuide
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import Foundation
import Suas

struct ArticlesReducer: Reducer {
    var initialState = ArticleList(articles: [], error: false)
    
    func reduce(state: ArticleList, action: Action) -> ArticleList? {

        if let action = action as? ArticlesFetchedAction {
            var newState = state
            newState.articles += action.articles
            newState.error = action.error
            return newState
        }
        
        //Pull to refresh
        if action is RefreshArticlesAction {
            var newState = state
            newState.articles = []
            newState.error = false
            return newState
        }
        
        return nil
    }
}

struct SelectedArticleReducer: Reducer {
    var initialState = SelectedArticle(article: ArticleItem())
    
    func reduce(state: SelectedArticle, action: Action) -> SelectedArticle? {
        
        if let action = action as? ViewArticleAction {
            var newState = state
            newState.article = action.article
            return newState
        }
        
        return nil
    }
}
