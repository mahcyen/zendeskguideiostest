//
//  State.swift
//  ZendeskGuide
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import Foundation

struct ArticleItem {
    var title: String?
    var content: String?
    var updatedDate: Date?
    var nextPage: String?
    
    init(title: String? = nil, content: String? = nil, updatedDate: Date? = nil, nextPage: String? = nil) {
        self.title = title
        self.content = content
        self.updatedDate = updatedDate
        self.nextPage = nextPage
    }
}

struct ArticleList {
    var articles: [ArticleItem]
    var error: Bool
}

struct SelectedArticle {
    var article: ArticleItem
}
