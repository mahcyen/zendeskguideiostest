//
//  ArticlesTableViewCell.swift
//  ZendeskGuide
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import UIKit

class ArticlesTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
