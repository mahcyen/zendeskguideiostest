//
//  ZendeskGuideApiTests.swift
//  ZendeskGuideApiTests
//
//  Created by Vince Mah on 24/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import XCTest
import Alamofire

class ZendeskGuideApiTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testValidCallToZendeskApiGetsHTTPStatusCode200() {
        // given
        let url = URL(string: "https://support.zendesk.com/api/v2/help_center/en-us/sections/200623776/articles.json")

        let promise = expectation(description: "Status code: 200")
        
        // when
        Alamofire.request(url!, method: .get).responseJSON { response in
            //then
            switch response.result {
            case .success( _):
                    if let statusCode = response.response?.statusCode {
                        if statusCode == 200 {
                            promise.fulfill()
                        } else {
                            XCTFail("Status code: \(statusCode)")
                        }
                    }
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }

        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
