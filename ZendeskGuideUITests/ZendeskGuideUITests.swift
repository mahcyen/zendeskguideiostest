//
//  ZendeskGuideUITests.swift
//  ZendeskGuideUITests
//
//  Created by Vince Mah on 23/01/2018.
//  Copyright © 2018 vmah. All rights reserved.
//

import XCTest

class ZendeskGuideUITests: XCTestCase {
    
    var app: XCUIApplication!
        
    override func setUp() {
        super.setUp()

        continueAfterFailure = false

        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewArticleDetail() {
        //given
        let articlesTableView = app.tables.element
        let articleCellFirst = app.cells.element(boundBy: 0)
        let articleDetailBackButton = app.navigationBars["ZendeskGuide.ArticleDetailView"].buttons["Articles"]

        //then
        let predicateNotEmpty = NSPredicate(format: "count > 0")
        expectation(for: predicateNotEmpty, evaluatedWith: articlesTableView.cells, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(articlesTableView.cells.count > 0)
        sleep(1)

        articleCellFirst.tap()
        sleep(2)
        XCTAssertTrue(articleDetailBackButton.exists)
        XCTAssertFalse(articlesTableView.exists)
        
        articleDetailBackButton.tap()
        sleep(2)
        XCTAssertTrue(articlesTableView.exists)
        XCTAssertFalse(articleDetailBackButton.exists)
    }
    
    func testPullToRefreshArticlesTableView() {
        //given
        let articlesTableView = app.tables.element
        let articleCellFirst = app.cells.element(boundBy: 0)
        let start = articleCellFirst.coordinate(withNormalizedOffset: (CGVector(dx: 0, dy: 0)))
        let finish = articleCellFirst.coordinate(withNormalizedOffset: (CGVector(dx: 0, dy: 6)))
        
        //then
        let predicateNotEmpty = NSPredicate(format: "count > 0")
        expectation(for: predicateNotEmpty, evaluatedWith: articlesTableView.cells, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(articlesTableView.cells.count > 0)
 
        start.press(forDuration: 0, thenDragTo: finish)
        
        expectation(for: predicateNotEmpty, evaluatedWith: articlesTableView.cells, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertFalse(articlesTableView.cells.count > 30)
    }
    
    func testScrollToLoadMoreArticlesTableView() {
        //given
        let articlesTableView = app.tables.element
        let articleCellFive = app.cells.element(boundBy: 4)
        
        //then
        let predicateNotEmpty = NSPredicate(format: "count > 0")
        expectation(for: predicateNotEmpty, evaluatedWith: articlesTableView.cells, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(articlesTableView.cells.count > 0)
        
        sleep(1)
        articleCellFive.swipeUp()
        
        let predicateMoreThan30 = NSPredicate(format: "count > 30")
        expectation(for: predicateMoreThan30, evaluatedWith: articlesTableView.cells, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(articlesTableView.cells.count > 30)
    }
}

